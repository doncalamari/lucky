var express = require('express');
var path = require('path');
var app = express();

app.get('/', function(req, res) {
  res.set('Content-Type', 'text/plain');
  res.sendFile(path.resolve('../src/index.html'));
});

app.post('/guess', function(req, res) {
  res.set('Content-Type', 'application/json');
  res.sendFile(path.resolve('data/guess.json'));
});

app.get('/getUserData', function(req, res) {
  res.set('Content-Type', 'application/json');
  res.sendFile(path.resolve('data/guessUserData.json'));
});

var server = app.listen(3000, function() {
  var host = server.address().address;
  var port = server.address().port;

  console.log('App listening at http://%s:%s', host, port);
});
