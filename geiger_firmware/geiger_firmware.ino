#define NUM_TIMES 4

volatile unsigned long times[NUM_TIMES];
volatile int counter = 0;
volatile bool led_flip_flag = false;
bool flip_flag = true;

uint8_t ledPin = A5;

void interrupt_handler() {
  Serial.print(".");
  led_flip_flag = !led_flip_flag;
  digitalWrite(ledPin, led_flip_flag);

  if (counter >= NUM_TIMES) {
    return;
  }

  unsigned long now = millis();

  if (now > times[counter]) {
    times[counter] = now;
  } else {
    counter = 0;
    initialize();
    return;
  }

  counter++;

}

void initialize() {
  // initialize with 0
  for (int i = 0; i < NUM_TIMES; i++) {
    times[i] = 0;
  }
}

void setup() {
  Serial.begin(9600);

  pinMode(2, INPUT);
  initialize();
  attachInterrupt(0, interrupt_handler, FALLING);

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
}

void loop() {

  if (NUM_TIMES == counter) {
    noInterrupts();
    long elapsed1, elapsed2 = 0;
    if (flip_flag) {
      elapsed1 = times[1] - times[0];
      elapsed2 = times[3] - times[2];
    } else {
      elapsed2 = times[1] - times[0];
      elapsed1 = times[3] - times[2];
    }

    if (elapsed1 != elapsed2) {
      if (elapsed1 > elapsed2) {
        Serial.print("0");
      } else {
        Serial.print("1");
      }
    }

    flip_flag = !flip_flag;
    counter = 0;
    interrupts();
  }


}
