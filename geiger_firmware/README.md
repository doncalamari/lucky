Sparkfun Geiger Counter Firmware
================================
Return a random number from the Sparkfun Geiger counter. https://www.sparkfun.com/products/11345

Details
-------
Open the sketch in the arduino IDE. Set the board type to Arduino Pro or mini. Set the processor type to ATMega328 (3.3v 8Mhz)

Upload sketch, then attach to the serial port at 9600 baud.
