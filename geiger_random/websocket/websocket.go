package websocket

import (
	"github.com/gorilla/websocket"
	"time"
)

type WebSocketConnection struct {
	Websocket *websocket.Conn
	Send      chan []byte
}

func (conn *WebSocketConnection) Reader() {

	for {
		time.Sleep(1 * time.Millisecond)
	}

	conn.Websocket.Close()
}

func (conn *WebSocketConnection) Writer() {
	for message := range conn.Send {
		err := conn.Websocket.WriteMessage(websocket.TextMessage, message)

		if err != nil {
			break
		}
	}

	conn.Websocket.Close()
}
