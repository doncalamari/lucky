package handler

import (
	"../handler_data"
	"../websocket"
	"encoding/json"
	"fmt"
	gorilla_websocket "github.com/gorilla/websocket"
	"log"
	"net/http"
	"text/template"
)

var test_template *template.Template
var server_status_template *template.Template
var h_data handler_data.Handler_data

var upgrader = &gorilla_websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(request *http.Request) bool {
		log.Println("Remote Address: ", request.RemoteAddr)
		return true
	},
}

func RunHandlers(html_location string, log_location string, new_h_data handler_data.Handler_data) {
	h_data = new_h_data
	init_templates(html_location)

	for {
		select {
		case reg := <-h_data.Register:
			h_data.Connections[reg] = true
		case un_reg := <-h_data.Unregister:
			if _, ok := h_data.Connections[un_reg]; ok {
				delete(h_data.Connections, un_reg)
				close(un_reg.Send)
			}
		case payload := <-h_data.Payload:

			for connection := range h_data.Connections {
				json, err := json.Marshal(payload)
				if err != nil {
					fmt.Println("error parsing struct: ", err)
				}
				select {
				case connection.Send <- json:
				default:
					delete(h_data.Connections, connection)
					close(connection.Send)
				}
			}

		}
	}
}

func init_templates(html_location string) {
	test_template = template.Must(template.ParseFiles(html_location + "test.html"))
	server_status_template = template.Must(template.ParseFiles(html_location + "status.html"))
}

func TestHandler(writer http.ResponseWriter, request *http.Request) {
	test_template.Execute(writer, request.Host)
}

func RandomWebServiceHandler(writer http.ResponseWriter, request *http.Request) {
	ws, err := upgrader.Upgrade(writer, request, nil)

	if err != nil {
		return
	}

	conn := &websocket.WebSocketConnection{Send: make(chan []byte, 256), Websocket: ws}
	h_data.Register <- conn
	defer func() {
		h_data.Unregister <- conn
	}()

	go conn.Writer()
	conn.Reader()
}

func ServerStatus(writer http.ResponseWriter, request *http.Request) {
	server_status_template.Execute(writer, request.Host)
}
