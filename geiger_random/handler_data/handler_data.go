package handler_data

import (
	"../serial"
	"../websocket"
)

type Handler_data struct {
	Connections map[*websocket.WebSocketConnection]bool
	Payload     chan serial.SerialPayload
	Register    chan *websocket.WebSocketConnection
	Unregister  chan *websocket.WebSocketConnection
}
