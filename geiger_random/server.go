package main

import (
	"./handler"
	"./handler_data"
	"./serial"
	"./websocket"
	"flag"
	"log"
	"net/http"
)

var (
	address       = flag.String("address", ":8080", "HTTP address and port to listen on")
	device_name   = flag.String("device", "/dev/ttyUSB0", "Serial device providing random numbers")
	server_id     = flag.String("server_id", "", "Unique identifier of this server")
	html_location = flag.String("html", "./", "Location of html files")
	log_location  = flag.String("log_location", "./", "Location of log files")
)

func main() {

	flag.Parse()
	if *server_id == "" {
		flag.Usage()
		log.Fatal("Server ID must be provided.")
	}

	var h_data handler_data.Handler_data = handler_data.Handler_data{
		Payload:     make(chan serial.SerialPayload),
		Register:    make(chan *websocket.WebSocketConnection),
		Unregister:  make(chan *websocket.WebSocketConnection),
		Connections: make(map[*websocket.WebSocketConnection]bool),
	}

	go serial.KeepReadingBytes(*device_name, 9600, *server_id, *log_location, h_data.Payload)
	go handler.RunHandlers(*html_location, *log_location, h_data)

	http.HandleFunc("/", handler.TestHandler)
	http.HandleFunc("/random_ws", handler.RandomWebServiceHandler)
	http.HandleFunc("/status", handler.ServerStatus)

	if err := http.ListenAndServe(*address, nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}

}
