package rfc3339time

import (
	"time"
)

type RFC3339Time struct {
	time.Time
}

func (t RFC3339Time) MarshalText() ([]byte, error) {
	return []byte(t.Format(time.RFC3339)), nil
}

func (t RFC3339Time) MarshalJSON() ([]byte, error) {
	return []byte(`"` + t.Format(time.RFC3339) + `"`), nil
}
