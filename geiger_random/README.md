Random Number Websocket Service
================================
Return a random number from the Sparkfun Geiger counter. https://www.sparkfun.com/products/11345
Replace the firmware on the device with the one in ../geiger_firmware.

Details
-------
To compile for the Raspberry pi either load the go compiler on your device or on another machine, run
```
GOARCH=arm GOOS=linux go build
```
