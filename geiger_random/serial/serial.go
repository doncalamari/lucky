package serial

import (
	"../archive"
	"../rfc3339time"
	"github.com/tarm/serial"
	"log"
	"time"
)

type SerialPayload struct {
	Value       byte
	Cpm         int
	Time_stamp  rfc3339time.RFC3339Time
	Error       error
	Total_found uint64
	Server_id   string
}

var total uint64 = 0

func KeepReadingBytes(device_name string, baud int, server_id string, log_location string, serial_payload_channel chan SerialPayload) {

	buffer := make([]byte, 1)
	payload := SerialPayload{
		Value:      254,
		Cpm:        0,
		Time_stamp: rfc3339time.RFC3339Time{time.Now()},
		Error:      nil,
		Server_id:  server_id,
	}

	cpm_start := time.Now()
	current_cpm := 0
	var serial *serial.Port
	for {
		var serial_error error
		if serial == nil {
			serial, serial_error = openSerialPort(device_name, baud)
			if serial_error != nil {
				time.Sleep(10 * time.Second)
				payload.Error = serial_error
				serial_payload_channel <- payload
				continue
			}
		}
		_, err := serial.Read(buffer)
		payload.Time_stamp = rfc3339time.RFC3339Time{time.Now()}
		if err != nil {
			log.Println("Error reading from device ", err)
			payload.Error = err
			serial_payload_channel <- payload
			serial.Close()
			serial = nil
			continue
		}

		if buffer[0] == 48 || buffer[0] == 49 {
			total = total + 1
			payload.Total_found = total
			payload.Value = buffer[0]
			go archive.ArchiveResult(payload.Value, payload.Time_stamp.Time, log_location)
			serial_payload_channel <- payload
		} else {

			now := time.Now()
			if now.Sub(cpm_start).Seconds() > 60 {
				payload.Cpm = current_cpm
				cpm_start = now
				current_cpm = 0
			} else {
				current_cpm = current_cpm + 1
			}
		}
	}

}

func openSerialPort(device_name string, baud int) (*serial.Port, error) {
	config := &serial.Config{Name: device_name, Baud: baud}
	serial, error := serial.OpenPort(config)
	if error != nil {
		log.Println("Error opening device "+device_name+": ", error)
	} else {
		log.Println("Opened device " + device_name)
	}
	return serial, error
}
