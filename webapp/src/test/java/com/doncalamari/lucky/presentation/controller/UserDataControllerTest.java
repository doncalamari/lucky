package com.doncalamari.lucky.presentation.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import com.doncalamari.lucky.model.User;
import com.doncalamari.lucky.model.UserGuessData;
import com.doncalamari.lucky.service.user.UserService;

public class UserDataControllerTest {

    private static final String USER_COOKIE = "userCookie";

    @Test
    public void testGuess() {
        final UserDataController userDataController = new UserDataController();
        final UserService userService = mock(UserService.class);
        final User user = new User();
        final UserGuessData storedUserGuessData = mock(UserGuessData.class);
        user.setUserGuessData(storedUserGuessData);
        when(userService.getUser(USER_COOKIE)).thenReturn(user);
        userDataController.setUserService(userService);

        final UserGuessData guess = userDataController.guess(USER_COOKIE);
        assertNotNull(guess);
        assertEquals("User guess data should be what is returned from the service.", storedUserGuessData, guess);
    }

}
