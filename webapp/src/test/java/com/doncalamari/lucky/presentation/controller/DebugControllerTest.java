package com.doncalamari.lucky.presentation.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DebugControllerTest {

    @Test
    public void testDebug() {
        final DebugController debugController = new DebugController();
        assertEquals("Should return the template name.", DebugController.TEMPLATE, debugController.debug());
    }

}
