package com.doncalamari.lucky.presentation.filter;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

public class AuthenticationFilterTest {

    @Test
    public void testDoFilter() throws IOException, ServletException {
        final AuthenticationFilter authenticationFilter = new AuthenticationFilter();

        final FilterChain chain = mock(FilterChain.class);
        final ServletResponse response = mock(HttpServletResponse.class);
        final HttpServletRequest request = mock(HttpServletRequest.class);

        final Cookie[] cookies = new Cookie[1];
        cookies[0] = new Cookie(AuthenticationFilter.USERID_COOKIE_NAME, "some value");
        Mockito.when(request.getCookies()).thenReturn(cookies);
        authenticationFilter.doFilter(request, response, chain);
        verify(chain).doFilter(request, response);
    }

    @Test
    public void testDoFilterCookieNotPresent() throws IOException, ServletException {
        final AuthenticationFilter authenticationFilter = new AuthenticationFilter();

        final FilterChain chain = mock(FilterChain.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final HttpServletRequest request = mock(HttpServletRequest.class);

        final Cookie[] cookies = new Cookie[1];
        cookies[0] = new Cookie("sadkjfkdj", "some value");
        Mockito.when(request.getCookies()).thenReturn(cookies);
        authenticationFilter.doFilter(request, response, chain);
        verify(chain).doFilter(request, response);
        verify(response).addCookie(any(Cookie.class));
    }

}
