package com.doncalamari.lucky.presentation.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Test;

import com.doncalamari.lucky.model.RandomPayload;
import com.doncalamari.lucky.service.RandomPayloadService;
import com.doncalamari.lucky.service.user.UserService;

public class RandomPayloadControllerTest {

    private static final String USERID = "someuserid";

    @Test
    public void testRandom() {
        final RandomPayloadController randomPayloadController = new RandomPayloadController();
        final RandomPayloadService service = mock(RandomPayloadService.class);
        final RandomPayload payload = mock(RandomPayload.class);
        when(service.retrievePayload(null)).thenReturn(payload);
        randomPayloadController.setRandomPayloadService(service);
        final RandomPayload controllerPayload = randomPayloadController.random();
        assertEquals(payload, controllerPayload);
    }

    @Test
    public void testGuess() {
        final RandomPayloadController randomPayloadController = new RandomPayloadController();
        final RandomPayloadService service = mock(RandomPayloadService.class);
        final RandomPayload payload = mock(RandomPayload.class);
        when(service.retrievePayload(null)).thenReturn(payload);
        randomPayloadController.setRandomPayloadService(service);
        final UserService userService = mock(UserService.class);
        randomPayloadController.setUserService(userService);

        randomPayloadController.guess(false, USERID);
        verify(userService).updateUserGuesses(eq(USERID), eq(false), eq(true), any(Date.class), any(Long.class));

        randomPayloadController.guess(true, USERID);
        verify(userService).updateUserGuesses(eq(USERID), eq(true), eq(false), any(Date.class), any(Long.class));

        when(payload.getValue()).thenReturn(Byte.valueOf("49"));

        randomPayloadController.guess(true, USERID);
        verify(userService).updateUserGuesses(eq(USERID), eq(true), eq(true), any(Date.class), any(Long.class));

        randomPayloadController.guess(false, USERID);
        verify(userService).updateUserGuesses(eq(USERID), eq(false), eq(false), any(Date.class), any(Long.class));
    }

}
