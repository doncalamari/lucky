package com.doncalamari.lucky.model;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

public class UserGuessDataTest {

    @Test
    public void testGetTotalGuessCounter() {
        final UserGuessData userData = new UserGuessData();

        userData.setFalseGuesses(Long.valueOf(10));
        userData.setTrueGuesses(Long.valueOf(12));
        assertEquals(Long.valueOf(22), userData.getTotalGuesses());
    }

    @Test
    public void testGetIncorrectCounter() {
        final UserGuessData userData = new UserGuessData();
        userData.setCorrectGuesses(Long.valueOf(12));
        userData.setFalseGuesses(Long.valueOf(10));
        userData.setTrueGuesses(Long.valueOf(12));
        assertEquals(Long.valueOf(10), userData.getIncorrectGuesses());
    }

    @Test
    public void testGetCorrectPercentage() {
        final UserGuessData userData = new UserGuessData();
        userData.setCorrectGuesses(Long.valueOf(10));
        userData.setFalseGuesses(Long.valueOf(10));
        userData.setTrueGuesses(Long.valueOf(10));
        assertEquals(new BigDecimal("0.5000000000"), userData.getCorrectPercentage());

        userData.setCorrectGuesses(Long.valueOf(3));
        userData.setFalseGuesses(Long.valueOf(5));
        userData.setTrueGuesses(Long.valueOf(5));
        assertEquals(new BigDecimal("0.3000000000"), userData.getCorrectPercentage());

        userData.setCorrectGuesses(Long.valueOf(1));
        userData.setFalseGuesses(Long.valueOf(2));
        userData.setTrueGuesses(Long.valueOf(1));
        assertEquals(new BigDecimal("0.3333333333"), userData.getCorrectPercentage());
    }

}
