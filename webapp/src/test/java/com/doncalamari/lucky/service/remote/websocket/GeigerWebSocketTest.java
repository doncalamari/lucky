package com.doncalamari.lucky.service.remote.websocket;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.websocket.api.Session;
import org.junit.Test;

import com.doncalamari.lucky.model.RandomPayload;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class GeigerWebSocketTest {

    private static final Date NOW = new Date();
    private static final BigInteger DUMMY_TOTAL = BigInteger.valueOf(1);
    private static final String DUMMY_TIMESTAMP = new SimpleDateFormat(RandomPayload.DATE_FORMAT, Locale.US).format(NOW);
    private static final Byte DUMMY_RANDOM_VALUE = Byte.valueOf("49");

    private static final String TEST_JSON = "{\"Value\":" + DUMMY_RANDOM_VALUE + ",\"Time_stamp\":\"" + DUMMY_TIMESTAMP
                    + "\",\"Error\":null," + "\"Total_found\":" + DUMMY_TOTAL + "}";

    @Test
    public void testEchoSocket() {
        final GeigerWebSocket webSocket = new GeigerWebSocket(null);
        assertNotNull(webSocket);
    }

    @Test
    public void testAwaitClose() throws InterruptedException {
        final GeigerWebSocket webSocket = new GeigerWebSocket(null);
        assertFalse(webSocket.awaitClose(1, TimeUnit.MILLISECONDS));
    }

    @Test
    public void testOnClose() {
        final GeigerWebSocket webSocket = new GeigerWebSocket(null);
        final long count = webSocket.getCloseLatch().getCount();
        webSocket.setSession(mock(Session.class));
        webSocket.onClose(0, "some reason");
        assertNull(webSocket.getSession());
        assertTrue(count > webSocket.getCloseLatch().getCount());
    }

    @Test
    public void testOnMessage() throws JsonParseException, JsonMappingException, IOException {
        final RandomPayload payloadArg = new RandomPayload();
        synchronized (payloadArg) {
            final GeigerWebSocket webSocket = new GeigerWebSocket(payloadArg);
            webSocket.onMessage(TEST_JSON);

            final RandomPayload payload = webSocket.getPayload();
            assertNull(payload.getError());
            assertEquals(DUMMY_RANDOM_VALUE, payload.getValue());
            assertEquals(RandomPayload.PayloadSource.GEIGER, payload.getSource());
            assertTrue(NOW.getTime() - payload.getDate().getTime() < 1000); // don't need nano-second precision
            assertEquals(DUMMY_TOTAL, payload.getTotalFound());
        }
    }

}
