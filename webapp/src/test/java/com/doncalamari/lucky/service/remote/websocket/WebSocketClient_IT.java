package com.doncalamari.lucky.service.remote.websocket;

import java.net.URI;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.doncalamari.lucky.model.RandomPayload;

public class WebSocketClient_IT {
    private static final String WEBSOCKET_URI = "ws://10.0.0.45:8080/random_ws";
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketClient_IT.class);

    @Test
    @Ignore
    public void testWebSocketClient() throws Exception {

        final WebSocketClient client = new WebSocketClient();
        final GeigerWebSocket socket = new GeigerWebSocket(new RandomPayload());
        try {
            client.start();

            client.connect(socket, new URI(WEBSOCKET_URI), new ClientUpgradeRequest());

            LOGGER.info("Connecting to : %s%n", WEBSOCKET_URI);

            socket.awaitClose(20, TimeUnit.SECONDS);
        } finally {
            client.stop();
        }

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("payload = " + socket.getPayload().toString());
        }
    }

}
