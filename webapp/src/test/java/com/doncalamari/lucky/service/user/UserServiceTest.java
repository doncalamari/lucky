package com.doncalamari.lucky.service.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Test;

import com.doncalamari.lucky.dao.UserDAO;
import com.doncalamari.lucky.model.User;
import com.doncalamari.lucky.model.UserGuessData;

public class UserServiceTest {

    private static final Long USER_ID = 1234l;
    private static final String COOKIE_VALUE = "cookieValue";

    @Test
    public void testUpdateUserGuessesNullInputs() {
        final UserService userService = new UserService();
        userService.setUserDao(mock(UserDAO.class));
        final String userId = null;
        Boolean guess = null;
        Boolean correct = null;
        final Date guessDate = null;
        final Long elapsedTime = null;
        try {
            userService.updateUserGuesses(userId, guess, correct, guessDate, elapsedTime);
            fail("Should throw exception.");
        } catch (final IllegalArgumentException ex) {
            // do nothing
        }

        guess = Boolean.FALSE;
        try {
            userService.updateUserGuesses(userId, guess, correct, guessDate, elapsedTime);
            fail("Should throw exception.");
        } catch (final IllegalArgumentException ex) {
            // do nothing
        }

        correct = Boolean.FALSE;
        try {
            userService.updateUserGuesses(userId, guess, correct, guessDate, elapsedTime);
            fail("Should throw exception.");
        } catch (final IllegalArgumentException ex) {
            // do nothing
        }
    }

    @Test
    public void testUpdateUserGuesses() {
        final UserService userService = new UserService();
        final UserDAO userDao = mock(UserDAO.class);
        userService.setUserDao(userDao);

        final User storedUser = new User();

        when(userDao.findByUserCookie(COOKIE_VALUE)).thenReturn(storedUser);

        final String userId = COOKIE_VALUE;
        Boolean guess = Boolean.FALSE;
        Boolean correct = Boolean.FALSE;
        Date guessDate = new Date(1);
        Long elapsedTime = Long.valueOf(0);

        UserGuessData userGuesses = userService.updateUserGuesses(userId, guess, correct, guessDate, elapsedTime);
        assertNotNull(userGuesses);
        assertEquals("Last date guessed should be the same one sent to function.", guessDate, userGuesses.getLastDateGuessed());
        assertFalse("Last guess should be false.", userGuesses.getLastGuess());
        assertEquals("Last time elapsed should be the same one sent to function.", elapsedTime,
                        userGuesses.getLastTimeElapsed());
        assertEquals("False guesses should increment.", Long.valueOf(1), userGuesses.getFalseGuesses());
        assertEquals("True guesses should not increment.", Long.valueOf(0), userGuesses.getTrueGuesses());
        assertEquals("Longest elapsed time should be 0.", Long.valueOf(0), userGuesses.getLongestTimeElapsed());

        guess = Boolean.TRUE;
        correct = Boolean.TRUE;
        guessDate = new Date(2);
        elapsedTime = Long.valueOf(1);

        userGuesses = userService.updateUserGuesses(userId, guess, correct, guessDate, elapsedTime);
        assertNotNull(userGuesses);
        assertEquals("Last date guessed should be the same one sent to function.", guessDate, userGuesses.getLastDateGuessed());
        assertTrue("Last guess should be true.", userGuesses.getLastGuess());
        assertEquals("Last time elapsed should be the same one sent to function.", elapsedTime,
                        userGuesses.getLastTimeElapsed());
        assertEquals("False guesses should not increment.", Long.valueOf(1), userGuesses.getFalseGuesses());
        assertEquals("True guesses should increment.", Long.valueOf(1), userGuesses.getTrueGuesses());
        assertEquals("Longest elapsed time should be the same one sent to function.", elapsedTime,
                        userGuesses.getLongestTimeElapsed());
    }

    @Test
    public void testGetUser() {
        final UserService userService = new UserService();
        final UserDAO userDao = mock(UserDAO.class);
        userService.setUserDao(userDao);

        User user = userService.getUser(COOKIE_VALUE);

        assertNotNull(user);
        verify(userDao).save(any(User.class));
        assertEquals("User cookie should be what was passed into function call.", COOKIE_VALUE, user.getUserCookie());

        final User storedUser = new User();
        storedUser.setId(USER_ID);
        reset(userDao);
        when(userDao.findByUserCookie(eq(COOKIE_VALUE))).thenReturn(storedUser);

        user = userService.getUser(COOKIE_VALUE);

        assertNotNull(user);
        verify(userDao, never()).save(any(User.class));
        assertEquals("User id should be what was passed into function call.", USER_ID, user.getId());
    }

}
