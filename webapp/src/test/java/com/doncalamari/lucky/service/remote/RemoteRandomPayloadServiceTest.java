package com.doncalamari.lucky.service.remote;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;

import org.apache.commons.lang3.NotImplementedException;
import org.junit.Test;

import com.doncalamari.lucky.model.RandomPayload;

public class RemoteRandomPayloadServiceTest {

    @Test
    public void testRetrievePayload() throws InterruptedException {
        final WebSocketTask websocketTask = mock(WebSocketTask.class);
        final RandomPayload mockPayload = mock(RandomPayload.class);
        when(websocketTask.getPayload()).thenReturn(mockPayload);
        final RemoteRandomPayloadService remoteRandomPayloadService = new RemoteRandomPayloadService(websocketTask);
        remoteRandomPayloadService.setWaitFlag(false);
        final RandomPayload payload = remoteRandomPayloadService.retrievePayload(null);
        assertNotNull(payload);
    }

    @Test(expected = NotImplementedException.class)
    public void testSavePayload() {
        final RemoteRandomPayloadService remoteRandomPayloadService = new RemoteRandomPayloadService(mock(WebSocketTask.class));
        remoteRandomPayloadService.savePayload(null);
    }

    @Test(expected = NotImplementedException.class)
    public void testListAll() {
        final RemoteRandomPayloadService remoteRandomPayloadService = new RemoteRandomPayloadService(mock(WebSocketTask.class));
        remoteRandomPayloadService.listAll();
    }

    @Test
    public void testInit() throws URISyntaxException {
        final RemoteRandomPayloadService remoteRandomPayloadService = new RemoteRandomPayloadService(mock(WebSocketTask.class));
        remoteRandomPayloadService.setWebsocketURI("ws://test.url");
        remoteRandomPayloadService.init();

    }
}
