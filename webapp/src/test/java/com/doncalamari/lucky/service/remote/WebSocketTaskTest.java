package com.doncalamari.lucky.service.remote;

import static org.junit.Assert.assertNotNull;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class WebSocketTaskTest {

    private URI testWebsocketUri;

    @Before
    public void setup() throws URISyntaxException {
        testWebsocketUri = new URI("ws://some.url");
    }

    @Test
    public void testWebSocketTask() throws URISyntaxException {
        final WebSocketTask webSocketTask = new WebSocketTask(testWebsocketUri);
        assertNotNull(webSocketTask);
    }

    @Test
    public void testRun() throws URISyntaxException {
        final WebSocketTask webSocketTask = new WebSocketTask(testWebsocketUri);
        webSocketTask.setClient(Mockito.mock(WebSocketClient.class));
        webSocketTask.setTerminate(true);
        webSocketTask.run();
    }

    @Test
    public void testGetPayload() {
        final WebSocketTask webSocketTask = new WebSocketTask(testWebsocketUri);
        assertNotNull(webSocketTask.getPayload());
    }

}
