package com.doncalamari.lucky.service.remote;

import java.net.URI;

import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.doncalamari.lucky.model.RandomPayload;
import com.doncalamari.lucky.service.remote.websocket.GeigerWebSocket;

public class WebSocketTask implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketTask.class);

    private final URI websocketURI;
    private final RandomPayload payload;
    private boolean terminate = false;
    private WebSocketClient client;
    private final GeigerWebSocket socket;
    private long delay = 1 * 1000;

    public WebSocketTask(final URI newWebsocketURI) {
        this.websocketURI = newWebsocketURI;
        client = new WebSocketClient();
        payload = new RandomPayload();
        socket = new GeigerWebSocket(payload);
    }

    @Override
    public void run() {

        try {
            client.start();
            client.connect(socket, websocketURI, new ClientUpgradeRequest());
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info("Connecting to: " + websocketURI);
            }

            while (!client.isFailed() && !this.terminate) {
                Thread.sleep(delay);
            }

        } catch (final Throwable t) {
            LOGGER.error("Error ", t);
        } finally {
            try {
                client.stop();
            } catch (final Exception e) {
                LOGGER.error("Error ", e);
            }
        }

    }

    public RandomPayload getPayload() {
        return payload;
    }

    void setTerminate(final boolean terminate) {
        this.terminate = terminate;
    }

    void setDelay(final long delay) {
        this.delay = delay;
    }

    void setClient(final WebSocketClient client) {
        this.client = client;
    }

}
