package com.doncalamari.lucky.service.remote;

import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.doncalamari.lucky.model.RandomPayload;
import com.doncalamari.lucky.service.RandomPayloadService;

@Component
@Singleton
public class RemoteRandomPayloadService implements RandomPayloadService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteRandomPayloadService.class);
    private WebSocketTask webSocketTask;

    @Value("${geiger.websocket.uri}")
    private String websocketURI;

    RemoteRandomPayloadService(final WebSocketTask testWebSocketTask) {
        this.webSocketTask = testWebSocketTask;
    }

    public RemoteRandomPayloadService() {
        // do nothing
    }

    private boolean waitFlag = true;

    @PostConstruct
    public void init() throws URISyntaxException {
        webSocketTask = new WebSocketTask(new URI(websocketURI));
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Starting thread to connect to " + websocketURI);
        }
        taskExecutor().execute(webSocketTask);
    }

    @Override
    public RandomPayload retrievePayload(final BigInteger id) {
        RandomPayload payload;
        synchronized (payload = webSocketTask.getPayload()) {
            try {
                if (waitFlag) {
                    payload.wait();
                }
                return new RandomPayload(payload);
            } catch (final InterruptedException e) {
                LOGGER.error("Interupted while waiting for notify.", e);
            }
        }
        return null;
    }

    @Override
    public void savePayload(final RandomPayload payload) {
        throw new NotImplementedException("Not implemented for remote service.");
    }

    @Override
    public List<RandomPayload> listAll() {
        throw new NotImplementedException("Not implemented for remote service.");
    }

    private ThreadPoolTaskExecutor taskExecutor() {
        final ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(1);
        pool.setMaxPoolSize(1);
        pool.setWaitForTasksToCompleteOnShutdown(true);
        pool.initialize();
        return pool;
    }

    void setWaitFlag(final boolean waitFlag) {
        this.waitFlag = waitFlag;
    }

    void setWebsocketURI(final String websocketURI) {
        this.websocketURI = websocketURI;
    }
}
