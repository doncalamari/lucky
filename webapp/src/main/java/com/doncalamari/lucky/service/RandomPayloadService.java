package com.doncalamari.lucky.service;

import java.math.BigInteger;
import java.util.List;

import com.doncalamari.lucky.model.RandomPayload;

public interface RandomPayloadService {

    RandomPayload retrievePayload(final BigInteger id);

    void savePayload(final RandomPayload payload);

    List<RandomPayload> listAll();
}
