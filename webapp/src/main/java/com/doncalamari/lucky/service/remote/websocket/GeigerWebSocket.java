package com.doncalamari.lucky.service.remote.websocket;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.doncalamari.lucky.model.RandomPayload;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebSocket(maxTextMessageSize = 64 * 1024)
public class GeigerWebSocket {
    private static final Logger LOGGER = LoggerFactory.getLogger(GeigerWebSocket.class);
    public static final Byte TRUE = Byte.valueOf("49");
    private final CountDownLatch closeLatch;

    private final RandomPayload payload;

    private Session session;

    public GeigerWebSocket(final RandomPayload payloadArg) {
        this.closeLatch = new CountDownLatch(1);
        this.payload = payloadArg;
    }

    public boolean awaitClose(final int duration, final TimeUnit unit) throws InterruptedException {
        return this.closeLatch.await(duration, unit);
    }

    @OnWebSocketConnect
    public void onConnect(final Session newSession) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Got connect: " + newSession);
        }
        this.session = newSession;
    }

    @OnWebSocketClose
    public void onClose(final int statusCode, final String reason) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Connection closed: " + Integer.valueOf(statusCode) + " - " + reason);
        }
        this.session = null;
        this.closeLatch.countDown();
    }

    @OnWebSocketMessage
    public void onMessage(final String message) throws JsonParseException, JsonMappingException, IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final RandomPayload jsonPayload = mapper.readValue(message, RandomPayload.class);
        synchronized (payload) {
            payload.setSource(RandomPayload.PayloadSource.GEIGER);
            payload.setDate(jsonPayload.getDate());
            payload.setError(jsonPayload.getError());
            payload.setValue(jsonPayload.getValue());
            payload.setTotalFound(jsonPayload.getTotalFound());
            payload.setServerId(jsonPayload.getServerId());
            payload.setCpm(jsonPayload.getCpm());

            payload.notifyAll();
        }

    }

    CountDownLatch getCloseLatch() {
        return closeLatch;
    }

    Session getSession() {
        return session;
    }

    void setSession(final Session session) {
        this.session = session;
    }

    public RandomPayload getPayload() {
        return payload;
    }
}
