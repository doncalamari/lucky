package com.doncalamari.lucky.service.user;

import java.util.Date;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.doncalamari.lucky.dao.UserDAO;
import com.doncalamari.lucky.model.User;
import com.doncalamari.lucky.model.UserGuessData;

@Component
@Singleton
public class UserService {
    @Autowired
    private UserDAO userDao;

    public UserGuessData updateUserGuesses(final String userId, final Boolean guess, final Boolean correct,
                    final Date guessDate, final Long elapsedTime) {
        if (guess == null || correct == null || elapsedTime == null) {
            throw new IllegalArgumentException("One of these arguments is null. guess = " + guess + ", correct = " + correct
                            + ", elapsedTime = " + elapsedTime);
        }

        final User user = getUser(userId);
        UserGuessData userGuessData = user.getUserGuessData();

        if (userGuessData == null) {
            userGuessData = new UserGuessData();
            userGuessData.setUser(user);
            user.setUserGuessData(userGuessData);
        }

        userGuessData.setLastDateGuessed(guessDate);
        userGuessData.setLastGuess(guess);
        userGuessData.setLastTimeElapsed(elapsedTime);

        if (correct) {
            userGuessData.setCorrectGuesses(userGuessData.getCorrectGuesses() + 1);
            userGuessData.setLastGuessCorrect(true);
            userGuessData.setLastDateCorrect(guessDate);
        } else {
            userGuessData.setLastGuessCorrect(false);
            userGuessData.setLastDateIncorrect(guessDate);
        }

        if (guess) {
            userGuessData.setTrueGuesses(userGuessData.getTrueGuesses() + 1);
        } else {
            userGuessData.setFalseGuesses(userGuessData.getFalseGuesses() + 1);
        }

        if (userGuessData.getLongestTimeElapsed() < elapsedTime) {
            userGuessData.setLongestTimeElapsed(elapsedTime);
        }

        userDao.save(user);
        return userGuessData;
    }

    public User getUser(final String userCookie) {
        User user = userDao.findByUserCookie(userCookie);

        if (user == null) {
            user = new User();
            user.setUserCookie(userCookie);
            user.setUserGuessData(new UserGuessData());
            userDao.save(user);
        }

        return user;
    }

    void setUserDao(final UserDAO userDao) {
        this.userDao = userDao;
    }

}
