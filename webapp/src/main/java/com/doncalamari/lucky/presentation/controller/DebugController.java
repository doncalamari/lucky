package com.doncalamari.lucky.presentation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DebugController {

    static final String TEMPLATE = "debug";

    @RequestMapping(method = RequestMethod.GET, value = "/debug")
    public String debug() {
        return TEMPLATE;
    }
}
