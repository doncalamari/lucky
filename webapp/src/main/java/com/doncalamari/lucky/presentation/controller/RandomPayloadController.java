package com.doncalamari.lucky.presentation.controller;

import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.doncalamari.lucky.model.RandomPayload;
import com.doncalamari.lucky.model.UserGuessData;
import com.doncalamari.lucky.service.RandomPayloadService;
import com.doncalamari.lucky.service.remote.websocket.GeigerWebSocket;
import com.doncalamari.lucky.service.user.UserService;

@RestController
public class RandomPayloadController {

    @Autowired
    private RandomPayloadService randomPayloadService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/random")
    public RandomPayload random() {
        return randomPayloadService.retrievePayload(null);
    }

    @RequestMapping(value = "/guess", method = RequestMethod.POST)
    public UserGuessData guess(final boolean guessValue, final String userCookie) {
        final Date now = new Date();

        final StopWatch timer = new StopWatch();
        timer.start();
        final Boolean value = randomPayloadService.retrievePayload(null).getValue().equals(GeigerWebSocket.TRUE);
        timer.stop();

        return userService.updateUserGuesses(userCookie, guessValue, value == guessValue, now, timer.getTime());
    }

    void setRandomPayloadService(final RandomPayloadService randomPayloadService) {
        this.randomPayloadService = randomPayloadService;
    }

    void setUserService(final UserService userService) {
        this.userService = userService;
    }
}
