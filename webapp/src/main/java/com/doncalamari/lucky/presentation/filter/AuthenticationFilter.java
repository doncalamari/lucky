package com.doncalamari.lucky.presentation.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

@Component
public class AuthenticationFilter implements Filter {
    static final String USERID_COOKIE_NAME = "userCookie";

    @Override
    public void destroy() {
        // do nothing
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
                    throws IOException, ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) request;

        try {
            Arrays.stream(httpRequest.getCookies()).filter(c -> c.getName().equals(USERID_COOKIE_NAME)).findFirst().get();
        } catch (final NoSuchElementException e) {
            final String userCookie = UUID.randomUUID().toString();
            ((HttpServletResponse) response).addCookie(new Cookie(USERID_COOKIE_NAME, userCookie));
        }

        chain.doFilter(httpRequest, response);
    }

    @Override
    public void init(final FilterConfig config) throws ServletException {
        // do nothing
    }

}
