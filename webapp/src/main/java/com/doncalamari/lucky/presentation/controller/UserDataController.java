package com.doncalamari.lucky.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.doncalamari.lucky.model.UserGuessData;
import com.doncalamari.lucky.service.user.UserService;

@RestController
public class UserDataController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/getUserData", method = RequestMethod.POST)
    public UserGuessData guess(final String userCookie) {
        return userService.getUser(userCookie).getUserGuessData();
    }

    void setUserService(final UserService userService) {
        this.userService = userService;
    }
}
