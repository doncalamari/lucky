package com.doncalamari.lucky.dao;

import org.springframework.data.repository.CrudRepository;

import com.doncalamari.lucky.model.User;

public interface UserDAO extends CrudRepository<User, Long> {
    User findByUserCookie(final String userCookie);
}
