package com.doncalamari.lucky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LuckyApplication {

    public static void main(final String... args) {
        SpringApplication.run(LuckyApplication.class, args);
    }
}
