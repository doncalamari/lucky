package com.doncalamari.lucky.model;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RandomPayload {
    // public static final String DATE_FORMAT = "yyyy-dd-MM'T'HH:mm:ss.SSSSSSSSS'Z'";
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX"; // RFC3339
    private BigInteger id;
    private Byte value;
    private Date date;
    private String error;
    private BigInteger totalFound;
    private PayloadSource source;
    private String serverId;
    private Integer cpm;

    public enum PayloadSource {
        GEIGER, PSEUDO
    }

    public RandomPayload() {
        super();
    }

    public RandomPayload(final RandomPayload randomPayload) {
        super();
        this.setDate(randomPayload.getDate());
        this.setError(randomPayload.getError());
        this.setId(randomPayload.getId());
        this.setValue(randomPayload.getValue());
        this.setTotalFound(randomPayload.getTotalFound());
        this.setCpm(randomPayload.getCpm());
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(final BigInteger id) {
        this.id = id;
    }

    public Byte getValue() {
        return value;
    }

    @JsonProperty("Value")
    public void setValue(final Byte value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    /**
     * Maybe instead of parsing the weird string we should change to a epoch based time.
     *
     * @throws ParseException
     */
    @JsonProperty("Time_stamp")
    public void setDateFromString(final String dateString) throws ParseException {
        this.date = new SimpleDateFormat(DATE_FORMAT, Locale.US).parse(dateString);
    }

    public String getError() {
        return error;
    }

    @JsonProperty("Error")
    public void setError(final String error) {
        this.error = error;
    }

    public BigInteger getTotalFound() {
        return totalFound;
    }

    @JsonProperty("Total_found")
    public void setTotalFound(final BigInteger totalFound) {
        this.totalFound = totalFound;
    }

    public PayloadSource getSource() {
        return source;
    }

    public void setSource(final PayloadSource source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("value", value).append("date", date).append("error", error)
                        .append("totalFound", totalFound).append("source", source).toString();
    }

    public String getServerId() {
        return serverId;
    }

    @JsonProperty("Server_id")
    public void setServerId(final String serverId) {
        this.serverId = serverId;
    }

    public Integer getCpm() {
        return cpm;
    }

    @JsonProperty("Cpm")
    public void setCpm(final Integer cpm) {
        this.cpm = cpm;
    }
}
