package com.doncalamari.lucky.model;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "user_guess_data")
public class UserGuessData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;
    private Boolean lastGuess;
    private Boolean lastGuessCorrect;
    private Date lastDateGuessed;
    private Date lastDateCorrect;
    private Date lastDateIncorrect;
    private Long correctGuesses = Long.valueOf(0);
    private Long trueGuesses = Long.valueOf(0);
    private Long falseGuesses = Long.valueOf(0);
    private Long lastTimeElapsed = Long.valueOf(0);
    private Long longestTimeElapsed = Long.valueOf(0);

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    public UserGuessData() {
        // do nothing
    }

    public Long getCorrectGuesses() {
        return correctGuesses;
    }

    public void setCorrectGuesses(final Long correctGuesses) {
        this.correctGuesses = correctGuesses;
    }

    public Long getTrueGuesses() {
        return trueGuesses;
    }

    public void setTrueGuesses(final Long trueGuesses) {
        this.trueGuesses = trueGuesses;
    }

    public Long getFalseGuesses() {
        return falseGuesses;
    }

    public void setFalseGuesses(final Long falseGuesses) {
        this.falseGuesses = falseGuesses;
    }

    public Boolean getLastGuess() {
        return lastGuess;
    }

    public void setLastGuess(final Boolean lastGuess) {
        this.lastGuess = lastGuess;
    }

    public Boolean getLastGuessCorrect() {
        return lastGuessCorrect;
    }

    public void setLastGuessCorrect(final Boolean lastGuessCorrect) {
        this.lastGuessCorrect = lastGuessCorrect;
    }

    public Date getLastDateGuessed() {
        return lastDateGuessed;
    }

    public void setLastDateGuessed(final Date lastDateGuessed) {
        this.lastDateGuessed = lastDateGuessed;
    }

    public Date getLastDateCorrect() {
        return lastDateCorrect;
    }

    public void setLastDateCorrect(final Date lastDateCorrect) {
        this.lastDateCorrect = lastDateCorrect;
    }

    public Date getLastDateIncorrect() {
        return lastDateIncorrect;
    }

    public void setLastDateIncorrect(final Date lastDateIncorrect) {
        this.lastDateIncorrect = lastDateIncorrect;
    }

    public Long getLastTimeElapsed() {
        return lastTimeElapsed;
    }

    public void setLastTimeElapsed(final Long lastTimeElapsed) {
        this.lastTimeElapsed = lastTimeElapsed;
    }

    public Long getLongestTimeElapsed() {
        return longestTimeElapsed;
    }

    public void setLongestTimeElapsed(final Long longestTimeElapsed) {
        this.longestTimeElapsed = longestTimeElapsed;
    }

    @JsonProperty("totalGuesses")
    public Long getTotalGuesses() {
        return trueGuesses + falseGuesses;
    }

    @JsonProperty("incorrectGuesses")
    public Long getIncorrectGuesses() {
        return getTotalGuesses() - correctGuesses;
    }

    @JsonProperty("correctPercentage")
    public BigDecimal getCorrectPercentage() {
        final BigDecimal divisor = new BigDecimal(getTotalGuesses(), MathContext.DECIMAL128);

        if (!BigDecimal.ZERO.equals(divisor)) {
            return new BigDecimal(correctGuesses, MathContext.DECIMAL128).divide(divisor, 10, RoundingMode.HALF_UP);
        }

        return BigDecimal.ZERO;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

}
