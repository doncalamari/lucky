package com.doncalamari.lucky.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;
    private String userCookie = StringUtils.EMPTY;
    private String externalUserId = StringUtils.EMPTY;
    private String nickname = StringUtils.EMPTY;
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private UserGuessData userGuessData;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getUserCookie() {
        return userCookie;
    }

    public void setUserCookie(final String userCookie) {
        this.userCookie = userCookie;
    }

    public String getExternalUserId() {
        return externalUserId;
    }

    public void setExternalUserId(final String externalUserId) {
        this.externalUserId = externalUserId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(final String nickname) {
        this.nickname = nickname;
    }

    public UserGuessData getUserGuessData() {
        return userGuessData;
    }

    public void setUserGuessData(final UserGuessData userGuessData) {
        this.userGuessData = userGuessData;
    }

}
