package com.doncalamari.lucky.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "history")
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;
    private String name;
    private Long trueValues = Long.valueOf(0);
    private Long falseValues = Long.valueOf(0);
    private Date lastEntry;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Long getTrueValues() {
        return trueValues;
    }

    public void setTrueValues(final Long trueValues) {
        this.trueValues = trueValues;
    }

    public Long getFalseValues() {
        return falseValues;
    }

    public void setFalseValues(final Long falseValues) {
        this.falseValues = falseValues;
    }

    public Date getLastEntry() {
        return lastEntry;
    }

    public void setLastEntry(final Date lastEntry) {
        this.lastEntry = lastEntry;
    }
}
